## AWS
- AWS Management Consloe : 관리 페이지
- AWS Region : 아시아 태평양(서울)
- Amazon EC2 : 클라우드의 가상 서버
- Amazon RDS : MySQL, PostgreSQL, Oracle, SQL Server 및 MariaDB를 위한 관계형의 관계형 데이터베이스 서비스

## 비용 확인(유료 서비스)
서비스 -> AWS비용관리 -> AWS Cost Explorer

## 인스턴스(서버) 시작
단계 1: Amazon Machine Image(AMI)선택
- Ubuntu Server 18.04 LTS (HVM), SSD Volume Type 선택
단계 2: 인스턴스 유형 선택
- General purpose t2.micro 선택
단계 7 : 인스턴스 시작 검토
- 시작하기
기존 키 페어 선택 또는 새 키 페어 생성
- 새 키 페어 생성
- 키 페어 다운로드
- 지금 인스턴스를 시작 중입니다. 확인
- 로컬 디스크(C:)>key>AWS에 저장
연결 -> 인스턴스에 연결 ->
키 페어 파일 속성 -> 보안 -> 고급 -> 상속 사용 안 함 -> Users가 들어간 이름 삭제
관리자들만 키페어를 사용할 수 있도록 권한 설정
cmd 관리자 모드로 접속 -> $ssh 입력 -> 서버 접속 성공

## Jupyter Notebook
- $sudo apt-get update
- $sudo apt-get install python3-pip
- $sudo pip3 install notebook
- $from notebook.auth import passwd
- $passwd()
- 비밀번호 설정 후 나오는 해쉬값 복사
- $jupyter notebook --generate-config
- $sudo vi 환경설정 파일 경로
- a 입력후 INSERT모드
- c = get_config()
- c.NotebookApp.password = u'해쉬값'
- c.NotebookApp.ip = '콘솔창 ip값'
- $sudo jupyter-notebook --allow-root

## 보안 그룹 설정
보안 그룹 -> 인바운드 편집 -> 규칙 추가 -> 유형 : Oracle RDS / 포트 범위 : 8888 / 소스 : 위치무관

## Jupyter Notebook 접속
URL IPv4 퍼블릭 IP:8888/ 입력
New -> Terminal

## Jupyter Notebook 설정
- 주피터 노트북 서버를 항상 실행 시키기 위한 설정
- ctrl + z 중지
- $bg
- $disown -h

## Jupyter Notebook 설정
- $sudo netstat -nap | grep 8888
- $sudo kill -9 프로세스 아이디
- 주피터 노트북 서버 중지
- $pwd
- $mkdir ssl
- $cd ssl
- $sudo openssl req -x509 -nodes -days 365 -newkey rsa:1024 -keyout "cert.key" -out "cert.pem" -batch
- ls
- $sudo vi 환경설정 파일 경로
- c.NotebookApp.certfile = u'/home/ubuntu/ssl/cert.pem'
- c.NotebookApp.keyfile = u'/home/ubuntu/ssl/cert.key'
- $sudo jupyter-notebook --allow-root
- https://IP 확인
- $disown -h
- https://IPv4 퍼블릭 IP:8888/ 입력
- https://13.209.98.172:8888

## Amazon RDS(데이터베이스 생성)
Oracle 선택
- RDS 프리 티어에 적용되는 옵션만 사용 체크
Oracle Edition
Oracle Enterprise Edition 선택
DB 엔진 버전
- Oracle 11.2.0.4v1
DB 인스턴스 식별자
- nailartdb
마스터 사용자 이름
- nailartmaster
마스터 암호
- lablab1123
데이터베이스 이름
- naildb
데이터베이스 포트
- 1521
퍼블릭 액세스 가능성
- 예
모니터링
- 확장 모니터링 사용 안 함
성능 개선 도우미
- 성능 개선 도우미 활성화
유지 관리
- 예
삭제 방지
- 삭제 방지 활성화
